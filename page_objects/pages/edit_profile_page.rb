class EditProfilePage < SitePrism::Page
  set_url '/myprofile/edit'

  # ELEMENTS
  section :header,         HeaderSection, '.mxd-header-inner'
  section :left_menu,      LeftMenuSection, :xpath, '//*[@id="root"]/div/div/div/div/div/section/div/nav/ul'
  section :user_info_form, UserInfoFormSection, :xpath, '//*[@id="root"]/div[1]/div/div/div/div/section/div/div/div[1]/div/form/div'

  element :save_user_info_btn,     :xpath, '//input[@data-webdriver="update-account-submit"]'
  element :save_user_email_btn,    :xpath, '//input[@data-webdriver="update-email-submit"]'
  element :save_user_password_btn, :xpath, '//input[@data-webdriver="update-password-submit"]'

  element :email_field,            '#email'
  element :password_field,         '#password'
  element :old_password_field,     '#old-password'
  element :new_password_field,     '#new-password'
end