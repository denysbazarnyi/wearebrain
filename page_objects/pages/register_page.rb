class RegisterPage < SitePrism::Page
  set_url '/register'

  # ELEMENTS
  section :header,         HeaderSection, '.mxd-header-inner'
  section :user_info_form, UserInfoFormSection, :xpath, '//*[@id="root"]/div/div/div/div/div/div/form/div[2]'

  element :submit_btn, '#submit'
end