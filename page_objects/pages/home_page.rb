class HomePage < SitePrism::Page
  set_url '/'

  # ELEMENTS
  section :header, HeaderSection, '.mxd-header-inner'
end