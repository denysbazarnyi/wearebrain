class MyProfilePage < SitePrism::Page
  set_url '/myprofile'

  # ELEMENTS
  section :header,    HeaderSection, '.mxd-header-inner'
  section :left_menu, LeftMenuSection, :xpath, '//*[@id="root"]/div/div/div/div/div/section/div/nav/ul'

  element :my_profile_content, '.mxd-content-section'
end