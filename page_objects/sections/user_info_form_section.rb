class UserInfoFormSection < SitePrism::Section

  # ELEMENTS

  element :gender_selector,       '#titleCode'
  element :first_name_field,      '#firstname'
  element :last_name_field,       '#lastname'
  element :address_field,         :xpath, '//input[@name="address"]'
  element :post_code_field,       '#postalCode'
  element :city_field,            '#town'
  element :email_field,           '#email'
  element :password_field,        '#password'
  element :tnc_checkbox,          :xpath, '//*[@id="root"]/div/div/div/div/div/div/form/div[2]/div/div[11]/div/div/label'

  # available only during edit
  element :phone_field,           '#phone'
  element :first_name_edit_field, '#first_name'
  element :last_name_edit_field,  '#last_name'

end