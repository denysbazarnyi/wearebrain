class HeaderSection < SitePrism::Section

  # ELEMENTS

  # for not logged in user
  element :register_link,   :xpath, '//a[@data-webdriver="registerlink"]'

  # for logged in user
  element :user_menu,       '.mxd-icon-user'
  element :my_profile_link, :xpath, '//a[@data-webdriver="mijn_profiel"]'
  element :logout_link,     :xpath, '//a[@data-webdriver="uitloggen"]'

end