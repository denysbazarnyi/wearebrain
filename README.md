# WeAreBrain Test Assignment

Public repo for the WeAreBrain test assignment:
Small test application to test User Profile edit on `https://www.acc.praxis.nl/voordemakers`

**Installation and running**

1. Clone the repo
2. Run `bundle install` to install required gems
3. Run `cucumber` in project folder to run tests

**Important notice**

This test application is not designed to execute exhaustive testing of predefined functionality. This is rather a demonstration of Test Application solution design approach. In real world you would find shorter Cucumber scenarios and more reusable steps as well as more detailed coverage.

**Selenium run screen cast**

https://vimeo.com/255778206

Thank you for your time!
Cheers!
