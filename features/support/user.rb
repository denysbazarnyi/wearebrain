require 'ffaker'

class User
  attr_accessor :email
  attr_accessor :password
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :city
  attr_accessor :address
  attr_accessor :zip

  def initialize
    @email =     FFaker::Internet.email
    @password =  FFaker::Internet.password
    @firstname = FFaker::Name.first_name
    @lastname =  FFaker::Name.last_name
    @city =      FFaker::Address.city
    @address =   FFaker::Address.street_address
    @zip =       '1234 AB'
  end
end