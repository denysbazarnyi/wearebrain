require 'cucumber'
require 'rspec/expectations'
require 'selenium-webdriver'
require 'capybara/cucumber'
require 'site_prism'

require 'yaml'
require 'require_all'

CONFIG = YAML.load_file("#{Dir.pwd}/config.yml")
ENVIRONMENT = ENV['RUN_ON'] || CONFIG['env']
STRINGS = YAML.load_file("#{Dir.pwd}/features/support/fixtures/string_values.yml")

Capybara.app_host = CONFIG[ENVIRONMENT]

require_all 'page_objects'

# setting Capybara driver
Capybara.default_driver = :selenium
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[window-size=1800,1000]))
end

After do
  # setting Capybara driver to reset sessions after all tests are done
  Capybara.reset_sessions!
end