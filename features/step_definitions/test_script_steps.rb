Given(/^I am unregistered visitor$/) do
  @home_page = HomePage.new
  @home_page.load
end


When(/^I register$/) do
  @home_page.header.register_link.click
  @register_page = RegisterPage.new

  @user = User.new

  @register_page.user_info_form.gender_selector.select 'Dhr.'
  @register_page.user_info_form.first_name_field.set    @user.firstname
  @register_page.user_info_form.last_name_field.set     @user.lastname
  @register_page.user_info_form.address_field.set       @user.address
  @register_page.user_info_form.post_code_field.set     @user.zip
  @register_page.user_info_form.city_field.set          @user.city
  @register_page.user_info_form.email_field.set         @user.email
  @register_page.user_info_form.password_field.set      @user.password
  @register_page.user_info_form.tnc_checkbox.click
  @register_page.submit_btn.click
  sleep 3
end

Then(/^I become registered and logged in user$/) do
  expect(@home_page.header).to have_user_menu

  @home_page.header.user_menu.hover
  sleep 0.5
  expect(@home_page.header).to have_my_profile_link
  expect(@home_page.header).to have_logout_link
end

When(/^I open my profile page$/) do
  @home_page.header.user_menu.hover
  sleep 0.5

  @home_page.header.my_profile_link.click
  sleep 0.5

  @my_profile_page = MyProfilePage.new
end

Then(/^I see respective user information$/) do
  expect(@my_profile_page.my_profile_content).to have_content @user.firstname
  expect(@my_profile_page.my_profile_content).to have_content @user.lastname
  expect(@my_profile_page.my_profile_content).to have_content @user.address
  expect(@my_profile_page.my_profile_content).to have_content @user.zip
  expect(@my_profile_page.my_profile_content).to have_content @user.city
  expect(@my_profile_page.my_profile_content).to have_content @user.email
end

When(/^I click on 'Edit Profile' link$/) do
  @my_profile_page.left_menu.edit_profile_link.click
  sleep 0.5

  @edit_profile_page = EditProfilePage.new
end

Then(/^I see 'Edit profile' page is openned$/) do
  expect(page.current_url).to include 'myprofile/edit'
end

When(/^I edit user '([^"]*)' in '([^"]*)' format$/) do |to_change, correctness|
  case to_change
  when 'user data'
    if correctness == 'wrong'
      @edit_profile_page.user_info_form.first_name_edit_field.set STRINGS['user']['correct']['first name']
      @edit_profile_page.user_info_form.last_name_edit_field.set  STRINGS['user']['correct']['last name']
      @edit_profile_page.user_info_form.address_field.set         STRINGS['user']['correct']['address']
      @edit_profile_page.user_info_form.post_code_field.set       STRINGS['user']['wrong']['zip']
      @edit_profile_page.user_info_form.city_field.set            STRINGS['user']['correct']['city']
      @edit_profile_page.user_info_form.phone_field.set           STRINGS['user']['wrong']['phone']
    else
      @edit_profile_page.user_info_form.first_name_edit_field.set STRINGS['user']['correct']['first name']
      @edit_profile_page.user_info_form.last_name_edit_field.set  STRINGS['user']['correct']['last name']
      @edit_profile_page.user_info_form.address_field.set         STRINGS['user']['correct']['address']
      @edit_profile_page.user_info_form.post_code_field.set       ''
      @edit_profile_page.user_info_form.post_code_field.set       STRINGS['user']['correct']['zip']
      @edit_profile_page.user_info_form.city_field.set            STRINGS['user']['correct']['city']
      @edit_profile_page.user_info_form.phone_field.set           STRINGS['user']['correct']['phone']
    end
  when 'email'
    if correctness == 'wrong'
      @edit_profile_page.email_field.set          STRINGS['user']['wrong']['email']
    else
      @new_email = FFaker::Internet.email
      @edit_profile_page.email_field.set          @new_email
      @edit_profile_page.password_field.set       @user.password
    end
    when 'password'
      if correctness == 'wrong'
        @edit_profile_page.old_password_field.set STRINGS['user']['wrong']['password']
        @edit_profile_page.new_password_field.set STRINGS['user']['correct']['password']
      else
        @edit_profile_page.old_password_field.set @user.password
        @edit_profile_page.new_password_field.set STRINGS['user']['correct']['password']
      end
  else
    raise STRINGS['errors']['data type']
  end
end

And(/^Try to save changes in '([^"]*)'$/) do |where|
  case where
  when 'user data'
    @edit_profile_page.save_user_info_btn.click
  when 'email'
    @edit_profile_page.save_user_email_btn.click
  when 'password'
    @edit_profile_page.save_user_password_btn.click
  else
    raise STRINGS['errors']['data type']
  end
  sleep 1
end

Then(/^I see that user '([^"]*)' was '([^"]*)' saved/) do |where, correctness|
  case where
    when 'user data'
    if correctness == 'not'
      expect(page).to have_content                                                STRINGS['user']['errors']['zip']
      expect(page).to have_content                                                STRINGS['user']['errors']['phone']
    else
      expect(page).to have_content                                                STRINGS['user']['confirmations']['user']
      expect(@edit_profile_page.user_info_form.first_name_edit_field.value).to eq STRINGS['user']['correct']['first name']
      expect(@edit_profile_page.user_info_form.last_name_edit_field.value).to eq  STRINGS['user']['correct']['last name']
      expect(@edit_profile_page.user_info_form.address_field.value).to eq         STRINGS['user']['correct']['address']
      expect(@edit_profile_page.user_info_form.post_code_field.value).to eq       STRINGS['user']['correct']['zip']
      expect(@edit_profile_page.user_info_form.city_field.value).to eq            STRINGS['user']['correct']['city']
      expect(@edit_profile_page.user_info_form.phone_field.value).to eq           STRINGS['user']['correct']['phone']
    end
  when 'email'
    if correctness == 'not'
      expect(page).to have_content                       STRINGS['user']['errors']['email']
      expect(page).to have_content                       STRINGS['user']['errors']['missing password']
    else
      expect(page).to have_content                       STRINGS['user']['confirmations']['email']
      expect(@edit_profile_page.email_field.value).to eq @new_email
    end
  when 'password'
    if correctness == 'not'
      expect(page).to have_content STRINGS['user']['errors']['incorrect password']
    else
      expect(page).to have_content STRINGS['user']['confirmations']['password']
    end
  else
    raise STRINGS['errors']['data type']
  end
end

