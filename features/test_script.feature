Feature: Wearebrain demo feature
  As a registered user
  I want to be able to edit user profile
  In order to store user information actualized

  Scenario: Unregistered visitor registers and edits his profile
    Given I am unregistered visitor

     When I register
     Then I become registered and logged in user

     When I open my profile page
     Then I see respective user information

     When I click on 'Edit Profile' link
     Then I see 'Edit profile' page is openned

     When I edit user 'user data' in 'wrong' format
      And Try to save changes in 'user data'
     Then I see that user 'user data' was 'not' saved

     When I edit user 'user data' in 'correct' format
      And Try to save changes in 'user data'
     Then I see that user 'user data' was 'correctly' saved

     When I edit user 'email' in 'wrong' format
      And Try to save changes in 'email'
     Then I see that user 'email' was 'not' saved

     When I edit user 'email' in 'correct' format
      And Try to save changes in 'email'
     Then I see that user 'email' was 'correctly' saved

     When I edit user 'password' in 'correct' format
      And Try to save changes in 'password'
     Then I see that user 'password' was 'correctly' saved